package main
import com.github.tototoshi.csv._
import scala.io.Source
import java.io._
import patmatch.Toint

object Main extends App {
  val reader = CSVReader.open(new File("sample.csv"))
  val full = reader.all()
  val (l1,l2) = isolate(full,0,1,Nil,Nil)
  println(l1,l2);
  val pt = new Toint(10, 20)

  // Move to a new location
  pt.move(10, 10);
  reader.close()

  def isolate(full: List[List[String]],frow:Int,lrow:Int,lp:List[Int],rp:List[Int]): (List[Int],List[Int]) = {
    val row = full.head
    val nrow = row.map(x => x.toInt)
    val fv = nrow(frow)
    val lv = nrow(lrow)

    if (full.length==1) {
      return (lp,rp)
    } else {
      return isolate(full.tail,frow,lrow,fv :: lp ,lv :: rp)
    }
  }
}

